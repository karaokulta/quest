from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='login_index'),
	path('<str:email>/<str:login_key>/', views.login, name='login_key'),
	path('mail_sent/', views.mail_sent, name='login_mail_sent'),
	path('logout/', views.logout, name='logout'),
]