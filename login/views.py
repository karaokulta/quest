from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from django.shortcuts import redirect
from django.utils.translation import gettext as _

# Helper functions
from random import randint
import uuid
from time import time
import hashlib

# Import from models
from main.models import User

def create_user(email):
	try:
		user = User.objects.get(email=email)

	except User.DoesNotExist:
		user = User(email=email, kind=User.NORMAL, current_points=0, login_key=str(uuid.uuid4()))
		user.save()

def update_login_key(email):
	try:
		user = User.objects.get(email=email)

		#login_key = str(uuid.uuid4())
		login_key = hashlib.sha256(str(uuid.uuid4()).encode('utf-8')).hexdigest()
		user.login_key = login_key
		user.save()

		return login_key
	except User.DoesNotExist:
		return ''

def send_mail(email, url):
	# Send mail
	from django.core.mail import send_mail, EmailMultiAlternatives
	from django.template.loader import get_template
	from django.template import Context

	template = get_template('login/mail_login_old.html')
	context = {
		'url': url
	}

	#subject, from_email = 'Inicia sesión en KO Quest', 'quest@karaokulta.com'
	subject, from_email = 'Inicia sesión en Gemugami Daily', 'daily@gemugami.com'

	to = [email]
	text_content = 'Read this with an HTML email client, please...'
	html_content = template.render(context)
	msg = EmailMultiAlternatives(subject, text_content, from_email, to)
	msg.attach_alternative(html_content, 'text/html')
	msg.send()

def index(request):
	if request.method == 'POST':
		#print(request.POST)
		email = request.POST['email']

		create_user(email)
		login_key = update_login_key(email)

		url = settings.BASE_URL + '/login/' + email + '/' + login_key + '/'
		send_mail(email, url)

		return redirect('login_mail_sent')

	if 'user_id' in request.session:
		return HttpResponse('There is session')
	else:
		return render(request, 'login/index.html')

	# TODO: Remove this or redirect
	return HttpResponse('Login ' + settings.BASE_URL)

def login(request, email, login_key):
	try:
		user = User.objects.get(email=email)

		if user.login_key == login_key:
			request.session['user_id'] = user.id
			update_login_key(email)

	except User.DoesNotExist:
		pass

	return redirect('index')

def logout(request):
	try:
		del request.session['user_id']
		request.session.flush()
	except KeyError:
		pass
	return redirect('index')

def mail_sent(request):
	return render(request, 'login/mail_sent.html')
