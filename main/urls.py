from django.urls import path
from . import views_main, views_init
from django.conf import settings

urlpatterns = [
	path('', views_main.index, name='index'),
	path('select_organization/', views_main.select_organization, name='main_select_organization'),
	path('daily/<int:organization_id>/', views_main.daily, name='main_daily'),
	path('user/<int:user_id>/', views_main.main_user, name='main_user'),
	path('success_in/', views_main.daily_success_in, name='daily_success_in'),
	path('success_out/', views_main.daily_success_out, name='daily_success_out'),
	#path('select_timezone/', views_main.select_timezone, name='main_select_timezone'),

	path('organizations/', views_main.show_organizations, name='main_show_organizations'),
	path('departments/<int:organization_id>/',
		views_main.show_departments, name='main_show_departments'),

	path('show_registries/org/<int:organization_id>/',
		views_main.show_registries, name='main_show_registries_by_organization'),
	path('show_registries/dep/<int:department_id>/',
		views_main.show_registries, name='main_show_registries_by_department'),
	path('registry/<int:registry_id>/',
		views_main.get_registry, name='main_get_registry'),

	# Add User Profile view
	# user/:user_id
]

# URLs unavailables on production
if not settings.PRODUCTION_MODE:
	urlpatterns.append(path('init', views_init.init_sample_data, name='main_init_sample_data'))