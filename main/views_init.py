from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
from django.conf import settings
from django.core.exceptions import FieldDoesNotExist, ObjectDoesNotExist

# Date and timezone tools
import datetime
import pytz # Timezone tools

# Import from models
from .models import Registry, User, Organization, Department, Registry

def init_sample_data(request):
	if settings.PRODUCTION_MODE:
		return redirect('index')

	# Remove all data
	Organization.objects.all().delete()
	Department.objects.all().delete()
	Registry.objects.all().delete()
	User.objects.all().delete()

	timezone_1 = 'America/Mexico_City'
	timezone_2 = 'America/Los_Angeles'

	# 1 - Create 2 organizations, KO and Sparkplug
	o_ko = Organization(
		name='KO',
		timezone=timezone_1,
		reminder_time=datetime.time(10, 0), # 10:00 AM
		late_time=datetime.time(10, 30), # 10:30 AM
	)
	o_ko.save()

	o_spark = Organization(
		name='Sparkplug Inc',
		timezone=timezone_2,
		reminder_time=datetime.time(13, 15), # 1:15 PM
		late_time=datetime.time(13, 30), # 1:30 PM
	)
	o_spark.save()

	o_uad = Organization(
		name='UAD',
		timezone=timezone_1,
		reminder_time=datetime.time(9, 00), # 9 AM
		late_time=datetime.time(23, 50), # 11:30 PM
	)
	o_uad.save()

	# 2 - Create 3 departments (KO-Production, Sparkplug-Production, KO-Mkt)
	d_ko_production = Department(
		name='KO - Production',
		organization=o_ko,
	)
	d_ko_production.save()

	d_ko_mkt = Department(
		name='KO - Marketing',
		organization=o_ko,
	)
	d_ko_mkt.save()

	d_spark_production = Department(
		name='Sparkplug - Production',
		organization=o_spark,
	)
	d_spark_production.save()

	d_uad_ldda = Department(
		name='UAD - LDDA',
		organization=o_uad,
	)
	d_uad_ldda.save()

	# 3 - Create 5 users (Alice, Bob, Charles, Daniel, Edd)
	u_1 = User(
		name='Alice',
		email='alice@ko.com',
		kind=User.NORMAL,
	)
	u_1.save()

	# We need to save first the simple user to be able to add Many-Many relationships
	u_1.organization.add(o_ko)
	u_1.department.add(d_ko_production)
	u_1.save()

	u_2 = User(
		name='Bob',
		email='bob@ko.com',
		kind=User.NORMAL,
	)
	u_2.save()

	u_2.organization.add(o_ko)
	u_2.department.add(d_ko_production)
	u_2.save()

	u_3 = User(
		name='Charles',
		email='charles@ko.com',
		kind=User.NORMAL,
	)
	u_3.save()

	u_3.organization.add(o_ko)
	u_3.department.add(d_ko_mkt)
	u_3.save()

	u_4 = User(
		name='Daniel',
		email='daniel@ko.com',
		kind=User.NORMAL,
	)
	u_4.save()

	u_4.organization.add(o_spark)
	u_4.department.add(d_spark_production)
	u_4.save()

	u_5 = User(
		name='Emmanuel A. Parada Licea',
		email='emmanuel@karaokulta.com',
		kind=User.NORMAL,
	)
	u_5.save()

	u_5.organization.add(o_ko)
	u_5.department.add(d_ko_production)
	u_5.save()

	u_6 = User(
		name='Anthony',
		email='antony@sparkplug.com',
		kind=User.NORMAL,
	)
	u_6.save()

	u_6.organization.add(o_spark)
	u_6.department.add(d_spark_production)
	u_6.save()

	# 4 - Log 3 different days (Friday, Monday, Tuesday)
	# 30ago2019, 2sep2019, 3sep2019
	o_ko_timezone = pytz.timezone(o_ko.timezone)
	o_spark_timezone = pytz.timezone(o_spark.timezone)

	# Convert this local time to UTC
	r_1_1_time = o_ko_timezone.localize(datetime.datetime(2019, 8, 30, 10, 15))
	print('30ago2019 10:15am in', timezone_1)
	print('Time:', r_1_1_time)
	r_1_1 = Registry(
		entry_1='30ago2019',
		entry_2='Another stuff',
		impediments_in='None',
		user=u_1,
		organization=o_ko,
		created_time=r_1_1_time
	)
	r_1_1.save()

	r_1_2_time = o_ko_timezone.localize(datetime.datetime(2019, 9, 2, 10, 10))
	r_1_2 = Registry(
		entry_1='2sep2019',
		impediments_in='None',
		user=u_1,
		organization=o_ko,
		created_time=r_1_2_time
	)
	r_1_2.save()

	r_1_3_time = o_ko_timezone.localize(datetime.datetime(2019, 9, 3, 10, 5))
	r_1_3 = Registry(
		entry_1='3sep2019',
		impediments_in='None',
		user=u_1,
		organization=o_ko,
		created_time=r_1_3_time
	)
	r_1_3.save()

	r_2_1_time = o_ko_timezone.localize(datetime.datetime(2019, 8, 30, 10, 15))
	r_2_1 = Registry(
		entry_1='30ago2019',
		entry_2='Something',
		impediments_in='Nothing',
		user=u_2,
		organization=o_ko,
		created_time=r_2_1_time
	)
	r_2_1.save()

	r_2_2_time = o_ko_timezone.localize(datetime.datetime(2019, 9, 2, 14, 15))
	r_2_2 = Registry(
		entry_1='2sep2019',
		entry_2='Something',
		impediments_in='Nothing',
		user=u_2,
		organization=o_ko,
		created_time=r_2_2_time
	)
	r_2_2.save()

	r_2_3_time = o_ko_timezone.localize(datetime.datetime(2019, 9, 3, 9, 50))
	r_2_3 = Registry(
		entry_1='3sep2019',
		impediments_in='-',
		user=u_2,
		organization=o_ko,
		created_time=r_2_3_time
	)
	r_2_3.save()

	r_6_1_time = o_spark_timezone.localize(datetime.datetime(2019, 9, 3, 9, 50))
	r_6_1 = Registry(
		entry_1='3sep2019',
		impediments_in='-',
		user=u_6,
		organization=o_spark,
		created_time=r_6_1_time
	)
	r_6_1.save()

	return HttpResponse('Data initialized')
