from django.db import models
from django.utils import timezone
import pytz # Timezone tools

class Organization(models.Model):
	name = models.CharField(max_length=200)
	logo = models.ImageField(blank=True, null=True)
	timezone = models.CharField(max_length=100)
	reminder_time = models.TimeField()
	late_time = models.TimeField()

	def __str__(self):
		return self.name

class Department(models.Model):
	name = models.CharField(max_length=200)
	organization = models.ForeignKey(Organization, on_delete=models.CASCADE)

	def __str__(self):
		return self.name

class User(models.Model):
	NORMAL = 0
	SUPER_USER = 1

	KIND = (
		(NORMAL, 'Normal'),
		(SUPER_USER, 'Super User'),
	)
	name = models.CharField(max_length=100)
	email = models.CharField(max_length=120, unique=True)
	avatar = models.ImageField(blank=True, null=True)
	kind = models.IntegerField(choices=KIND)
	current_points = models.IntegerField(default=0)
	level = models.IntegerField(default=1)
	streak = models.IntegerField(default=0)
	login_key = models.CharField(max_length=120, editable=False)
	organization = models.ManyToManyField(Organization)
	department = models.ManyToManyField(Department)

	def __str__(self):
		return self.email

class Registry(models.Model):
	class Meta:
		verbose_name_plural = "registries"

	OPEN = 0
	CLOSED_ON_TIME = 1
	CLOSED_OUT_OF_TIME = 2

	STATUS = (
		(OPEN, 'Open'), # White
		(CLOSED_ON_TIME, 'Closed on time'), # Green
		(CLOSED_OUT_OF_TIME, 'Closed out of time'), # Red
	)
	status = models.IntegerField(choices=STATUS, default=OPEN)

	user = models.ForeignKey(User, on_delete=models.CASCADE)
	organization = models.ForeignKey(Organization, on_delete=models.CASCADE)

	created_time = models.DateTimeField()
	modified = models.DateTimeField()

	# TODO: Remove this boolean when status is implemented
	is_closed = models.BooleanField(default=False)
	closed_time = models.DateTimeField(blank=True, null=True)

	# Current day
	entry_1 = models.CharField(max_length=1000)
	entry_1_done = models.BooleanField(default=False)
	entry_2 = models.CharField(max_length=1000, blank=True)
	entry_2_done = models.BooleanField(default=False)
	entry_3 = models.CharField(max_length=1000, blank=True)
	entry_3_done = models.BooleanField(default=False)
	entry_4 = models.CharField(max_length=1000, blank=True)
	entry_4_done = models.BooleanField(default=False)
	entry_5 = models.CharField(max_length=1000, blank=True)
	entry_5_done = models.BooleanField(default=False)
	entry_6 = models.CharField(max_length=1000, blank=True)
	entry_6_done = models.BooleanField(default=False)
	entry_7 = models.CharField(max_length=1000, blank=True)
	entry_7_done = models.BooleanField(default=False)
	entry_8 = models.CharField(max_length=1000, blank=True)
	entry_8_done = models.BooleanField(default=False)
	entry_9 = models.CharField(max_length=1000, blank=True)
	entry_9_done = models.BooleanField(default=False)
	entry_10 = models.CharField(max_length=1000, blank=True)
	entry_10_done = models.BooleanField(default=False)

	feelings_emoji = models.CharField(max_length=10, default='')
	feelings = models.CharField(max_length=1000, default='')
	# impediments_in = models.CharField(max_length=1000, default='')
	impediments_out = models.CharField(max_length=1000, default='', blank=True)

	# Next day
	next_day_entry_1 = models.CharField(max_length=1000, blank=True)
	next_day_entry_2 = models.CharField(max_length=1000, blank=True)
	next_day_entry_3 = models.CharField(max_length=1000, blank=True)
	next_day_entry_4 = models.CharField(max_length=1000, blank=True)
	next_day_entry_5 = models.CharField(max_length=1000, blank=True)
	next_day_entry_6 = models.CharField(max_length=1000, blank=True)
	next_day_entry_7 = models.CharField(max_length=1000, blank=True)
	next_day_entry_8 = models.CharField(max_length=1000, blank=True)
	next_day_entry_9 = models.CharField(max_length=1000, blank=True)
	next_day_entry_10 = models.CharField(max_length=1000, blank=True)

	def save(self, *args, **kwargs):
		''' On save, update timestamps '''
		#if not self.id:
			#self.created = timezone.now()

		self.modified = timezone.now()
		return super(Registry, self).save(*args, **kwargs)

	def __str__(self):
		# Example: eapl.mx@gmail.com - 02/Sep/2019 04:25 AM
		# IMPORTANT: Depends on a valid Timezone
		# TODO: Have a callback if the zone is not valid
		if self.organization.timezone in pytz.all_timezones:
			time = self.created_time.astimezone(pytz.timezone(self.organization.timezone))
		else:
			time = self.created_time

		if self.is_closed:
			closed_str = 'Closed'
		else:
			closed_str = 'NOT Closed'

		return f'{self.user} - {closed_str} - {time:%d/%b/%Y %I:%M %p %Z}'
