# Generated by Django 2.2.1 on 2019-09-19 18:40

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_user_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='late_time',
            field=models.TimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='organization',
            name='reminder_time',
            field=models.TimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='registry',
            name='feelings',
            field=models.CharField(default='', max_length=1000),
        ),
    ]
