from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
from django.core.exceptions import FieldDoesNotExist, ObjectDoesNotExist
from django.conf import settings
from django.utils.datastructures import MultiValueDictKeyError

# Math tools
import math

# Timezone tools
import datetime
from django.utils import timezone
import pytz

# Import from models
from .models import Registry, User, Organization, Department, Registry

def is_same_day(day_1: datetime, day_2: datetime, tz_string: str) -> bool:
	# Get the ISO day number to check if they were made same day
	# For organization's timezone

	# Check if the following is needed when Django is outside of UTC Timezone
	# We are asumming that everything is working on UTC
	#timezone.activate(pytz.timezone(tz_string))
	#timezone.localtime(timezone.now())

	day_1_iso = day_1.astimezone(pytz.timezone(tz_string)).__format__('%j')
	day_2_iso = day_2.astimezone(pytz.timezone(tz_string)).__format__('%j')

	return day_1_iso == day_2_iso

def is_logged_in(request):
	return 'user_id' in request.session

def index(request):
	if not is_logged_in(request):
		return redirect('login_index')

	if 'user_id' in request.session:
		# Get the user to start working with it
		try:
			# Get the registries for a user
			u = User.objects.get(pk=request.session['user_id'])
		except ObjectDoesNotExist:
			print('The user in session does not exist')
			return redirect('logout')

		# If the user has more than one organization, redirect to a selector.
		# Otherwise, select the only one as a default
		organizations_num = u.organization.count()
		#print('# of organizations:', organizations_num)

		if organizations_num == 1:
			# TODO: The user has 1 organization
			o = u.organization.all()[:1].get()
			#print('User\'s Organization:', o)
			#print('Organization ID', o.id)
			# Redirect to fill their daily with that organization
			return redirect('main_daily', o.id)
		if organizations_num == 0 or organizations_num > 1:
			return redirect('main_select_organization')
			# TODO: Redirect to an organization selector
			#return HttpResponse('Here goes an organization Selector')

	return redirect('login_index')

def is_registry_on_time(registry, organization):
	""" Returns a boolean to define if the registry is between
	the organization parameters to be on time."""
	tz = organization.timezone
	tzinfo = pytz.timezone(tz)
	created_time = registry.created_time.astimezone(tzinfo)
	closed_time = registry.closed_time.astimezone(tzinfo)
	#print('Created time', created_time)
	#print('Closed time', closed_time)

	# The created_time should be at lease one day after the last registry
	# and today before the organization's late_time
	# Convert from a time object to a today's datetime
	# https://stackoverflow.com/a/36623787
	#print('Org late time', organization.late_time)
	today_late_time_naive = datetime.datetime.combine(datetime.datetime.today(),
		organization.late_time)
	today_late_time = tzinfo.localize(today_late_time_naive)
	#print('Today late time', today_late_time)

	created_on_time = created_time < today_late_time
	#print('Created on time', created_on_time)

	# Check that the closed time is the same day than the created time.
	closed_on_time = is_same_day(created_time, timezone.now(), tz)

	return created_on_time and closed_on_time

# Write your daily in or daily out
def daily(request, organization_id):
	if request.method == 'POST':
		# Receive the POST request and save to DB
		print(request.POST)

		if 'out' in request.POST:
			# If it's a Daily Out, update the current registry
			user = User.objects.get(pk=request.session['user_id'])
			last_registry = Registry.objects.filter(user=user).order_by('-created_time')[:1][0]
			organization = Organization.objects.get(pk=organization_id)

			last_registry.entry_1_done = '1_done' in request.POST
			last_registry.entry_2_done = '2_done' in request.POST
			last_registry.entry_3_done = '3_done' in request.POST
			last_registry.entry_4_done = '4_done' in request.POST
			last_registry.entry_5_done = '5_done' in request.POST
			last_registry.entry_6_done = '6_done' in request.POST
			last_registry.entry_7_done = '7_done' in request.POST
			last_registry.entry_8_done = '8_done' in request.POST
			last_registry.entry_9_done = '9_done' in request.POST
			last_registry.entry_10_done = '10_done' in request.POST
			
			last_registry.impediments_out = request.POST['impediments_out']

			try:
				last_registry.next_day_entry_1 = request.POST['1']
			except MultiValueDictKeyError:
				last_registry.next_day_entry_1 = ""
			try:
				last_registry.next_day_entry_2 = request.POST['2']
			except MultiValueDictKeyError:
				last_registry.next_day_entry_2 = ""
			try:
				last_registry.next_day_entry_3 = request.POST['3']
			except MultiValueDictKeyError:
				last_registry.next_day_entry_3 = ""
			try:
				last_registry.next_day_entry_4 = request.POST['4']
			except MultiValueDictKeyError:
				last_registry.next_day_entry_4 = ""
			try:
				last_registry.next_day_entry_5 = request.POST['5']
			except MultiValueDictKeyError:
				last_registry.next_day_entry_5 = ""
			try:
				last_registry.next_day_entry_6 = request.POST['6']
			except MultiValueDictKeyError:
				last_registry.next_day_entry_6 = ""
			try:
				last_registry.next_day_entry_7 = request.POST['7']
			except MultiValueDictKeyError:
				last_registry.next_day_entry_7 = ""
			try:
				last_registry.next_day_entry_8 = request.POST['8']
			except MultiValueDictKeyError:
				last_registry.next_day_entry_8 = ""
			try:
				last_registry.next_day_entry_9 = request.POST['9']
			except MultiValueDictKeyError:
				last_registry.next_day_entry_9 = ""
			try:
				last_registry.next_day_entry_10 = request.POST['10']
			except MultiValueDictKeyError:
				last_registry.next_day_entry_10 = ""

			last_registry.is_closed = True
			last_registry.closed_time=timezone.now()

			# TODO: Check the time for the closed
			# If created_time and closed_time are within the allowed range,
			# mark it as closed_on_time. Otherwise closed_out_of_time
			if is_registry_on_time(last_registry, organization):
				last_registry.status = Registry.CLOSED_ON_TIME
				user.streak += 1
				user.save()
			else:
				last_registry.status = Registry.CLOSED_OUT_OF_TIME
				user.streak = 0
				user.save()
			last_registry.save()

			# Redirecting to Index to check if has finished for today,
			# had a non closed Daily Out
			return redirect('index')
		else:
			# Check if was received a Daily In or a Daily Out
			# Expected POST fields for Daily In
			try:
				entry_1 = request.POST['1']
			except MultiValueDictKeyError:
				entry_1 = ""

			try:
				entry_2 = request.POST['2']
			except MultiValueDictKeyError:
				entry_2 = ""

			try:
				entry_3 = request.POST['3']
			except MultiValueDictKeyError:
				entry_3 = ""
			
			try:
				entry_4 = request.POST['4']
			except MultiValueDictKeyError:
				entry_4 = ""
			
			try:
				entry_5 = request.POST['5']
			except MultiValueDictKeyError:
				entry_5 = ""
			
			try:
				entry_6 = request.POST['6']
			except MultiValueDictKeyError:
				entry_6 = ""
			
			try:
				entry_7 = request.POST['7']
			except MultiValueDictKeyError:
				entry_7 = ""

			try:
				entry_8 = request.POST['8']
			except:
				entry_8 = ""

			try:
				entry_9 = request.POST['9']
			except MultiValueDictKeyError:
				entry_9 = ""

			try:
				entry_10 = request.POST['10']
			except MultiValueDictKeyError:
				entry_10 = ""

			feelings = request.POST['feelings']
			feelings_emoji = request.POST['feelings_emoji']
			# impediments_in = request.POST['impediments_in']

			user = User.objects.get(pk=request.session['user_id'])
			
			# If it's a Daily In
			r = Registry(
				entry_1=entry_1,
				entry_2=entry_2,
				entry_3=entry_3,
				entry_4=entry_4,
				entry_5=entry_5,
				entry_6=entry_6,
				entry_7=entry_7,
				entry_8=entry_8,
				entry_9=entry_9,
				entry_10=entry_10,
				feelings=feelings,
				feelings_emoji=feelings_emoji,
				# impediments_in=impediments_in,
				created_time=timezone.now(),
				user=user,
				# TODO: Check that is a valid organization, because this
				# comes from a user
				organization=Organization.objects.get(pk=organization_id),
			)
			r.save()

			user.current_points += 10
			user.level = math.floor(math.log10(user.current_points) + 1.5)
			user.save()

			# Thank you. Please come at the end of your day to Check out.
			return redirect('daily_success_in')

	# Check if the current user already has sent their daily for today

	# Get the last registry for this player (if any) and check if it's from
	# the same day (from organization perspective)
	user = User.objects.get(pk=request.session['user_id'])
	o = Organization.objects.get(pk=organization_id)
	#timezone.activate(pytz.timezone(o.timezone))

	# TODO: Check if there is a previous registry
	if Registry.objects.filter(user=user).count() > 0:
		last_registry = Registry.objects.filter(user=user).order_by('-created_time')[:1][0]
		#print(last_registry)

		if last_registry.is_closed:
			print('Last registry was closed')
			# The registry is closed
			# If it's the same day -> Just show a "Thank you message"
			# return redirect('daily_success_in')
			# If the last registry is from yesterday -> Show
			# Redirect to next day, but check if it's from yesterday

			if is_same_day(timezone.now(), last_registry.created_time, o.timezone):
				print('Last registry was done today')
				return redirect('daily_success_out')
			else:
				print('Last registry was NOT done today')

			render_data = {
				'last_registry': last_registry,
			}
			return render(request, 'main/daily_in.html', render_data)
		else: # Not closed
			print('Last registry not closed')
			# Redirect to daily_out
			render_data = {
				'last_registry': last_registry,
			}
			return render(request, 'main/daily_out.html', render_data)

	# There was no last registry, show the default Daily In view
	print('There is not last registry')
	return render(request, 'main/daily_in.html')

def select_organization(request):
	if not is_logged_in(request):
		return redirect('login_index')

	u = User.objects.get(pk=request.session['user_id'])
	render_data = {
		'organizations': u.organization.all()
	}

	return render(request, 'main/select_organization.html', render_data)

def main_user(request, user_id):
	return HttpResponse('Hello ' + str(user_id))

def daily_success_in(request):
	return render(request, 'main/success_in.html')

def daily_success_out(request):
	return render(request, 'main/success_out.html')

def select_timezone(request):
	timezone = 'America/Mexico_City'

	if timezone in pytz.all_timezones:
		print(timezone, 'exists')
	else:
		print(timezone, 'doesn\'t exist')

	return render(request, 'main/select_timezone.html')

def show_organizations(request):
	if not is_logged_in(request):
		return redirect('login_index')

	# List all the organizations in the system
	u = User.objects.get(pk=request.session['user_id'])

	orgs = u.organization.all()
	# If there is just one organization, move directly to that org
	#print('Organizations num', orgs.count())
	if orgs.count() == 1:
		#print(orgs[0].id)
		return redirect('main_show_departments', orgs[0].id)

	render_data = {
		'organizations': orgs
	}
	return render(request, 'main/show_organizations.html', render_data)

def show_departments(request, organization_id):
	# TODO: Change organizations to departments
	render_data = {
		'organization': Organization.objects.get(pk=organization_id),
		'departments': Department.objects.filter(organization=organization_id),
	}
	print(render_data)
	return render(request, 'main/show_departments.html', render_data)

def show_daily_rate(is_closed_info, users):
	total_registries_of_day = len(is_closed_info)
	number_of_users = len(users)
	closed_dailies = 0
	for x in is_closed_info:
		if x:
			closed_dailies += 1

	opened_percentage = round((total_registries_of_day/number_of_users)*100, 1)
	closed_percentage = round((closed_dailies/number_of_users)*100, 1)
	result = {
		"total_users": number_of_users,
		"dailies_opened": total_registries_of_day,
		"opened_percentage": str(opened_percentage) + "%" ,
		"dailies_closed": closed_dailies,
		"closed_percentage": str(closed_percentage) + "%",
		"date": datetime.date.today()
	}

	return result

def show_registries(request, organization_id=None, department_id=None):
	if not is_logged_in(request):
		return redirect('login_index')

	if organization_id != None:
		# Get the latest registries for that organization
		o = Organization.objects.get(pk=organization_id)
		timezone.activate(pytz.timezone(o.timezone))
		# TODO: Check if that organization exists

		registries_in_organization = {}
		try:
			# Issue with get and filter
			# https://stackoverflow.com/a/22064255
			# Get the registries for that organization
			registries_in_organization = Registry.objects.filter(organization=o).order_by('-created_time')
			print("HERE IS THE DATA YOU ASKED FOR")
			#print('Registries', registries_in_organization)
			print(registries_in_organization)
		except ObjectDoesNotExist:
			print('No registries for this Organization')

		render_data = {
			'organization': o,
			'registries': registries_in_organization,
		}

		return render(request, 'main/show_registries.html', render_data)
	if department_id != None:
		d = Department.objects.get(pk=department_id)
		# Get all the users for that department
		users = User.objects.filter(department=department_id)
		print('Users in this department', users)

		registries_in_department = Registry.objects.filter(user__in=list(users)).order_by('-created_time')
		registries_is_closed_info = Registry.objects.filter(created_time__range=[str(datetime.date.today()) + " 00:00:01", str(datetime.date.today()) + " 23:59:59"]).filter(user__in=list(users)).order_by('-created_time').values_list("is_closed", flat=True)
		# TODO: Fix an strange bug related with activating the Timezone
		# When you print the registries, the correct timezone fixes
		# Try commenting the next line, and check the __str__ for Registry mode

		print('Registries for this department', registries_in_department)
		# print(str(datetime.date.today()) + " 00:00:01")
		print("HERE IS THE DATA YOU ASKED FOR")
		daily_rate = show_daily_rate(registries_is_closed_info, users)

		print(daily_rate)
		render_data = {
			'department': d,
			'registries': registries_in_department,
			'STATIC_ROOT': settings.STATIC_ROOT,
			"daily_rate": daily_rate 
		}

		return render(request, 'main/show_registries.html', render_data)

def get_registry(request, registry_id):
	if not is_logged_in(request):
		return redirect('login_index')

	render_data = {
		'registry': Registry.objects.get(pk=registry_id),
		'STATIC_ROOT': settings.STATIC_ROOT,
	}

	return render(request, 'main/get_registry.html', render_data)
