from django.contrib import admin
from .models import Organization, Department, Registry, User

admin.site.register(Organization)
admin.site.register(Department)
admin.site.register(Registry)
admin.site.register(User)