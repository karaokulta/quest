# quest
A gamified web app to promote writing your daily scrum

# Resources
http://mishguruorg.github.io/angular-timezone-selector/
https://docs.djangoproject.com/en/2.2/topics/db/queries/
https://docs.djangoproject.com/en/2.2/topics/i18n/timezones/
https://docs.djangoproject.com/en/2.2/ref/templates/builtins/#date
https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Deployment
https://www.digitalocean.com/community/tutorials/how-to-set-up-nginx-server-blocks-virtual-hosts-on-ubuntu-16-04
https://www.abidibo.net/blog/2016/10/07/securing-django-lets-encrypt/
https://docs.djangoproject.com/en/2.2/topics/security/
https://community.letsencrypt.org/t/type-unauthorized-detail-invalid-response-from/36183/2
https://www.agiliq.com/blog/2013/08/minimal-nginx-and-gunicorn-configuration-for-djang/
https://www.hostinger.com/tutorials/iptables-tutorial

# Quick commands
manage.py createsuperuser
django-admin makemessages -a
django-admin compilemessages

nginx -s reload

# Start Gunicorn in Daemon mode
gunicorn quest.wsgi --daemon

# Kill Gunicorn
pkill gunicorn

# Deployment
1. Setup virtualenv for your server

2. Install the requirements

