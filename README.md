# daily-quest
A gamified web app to promote writing your Daily Scrum

# Setup
# Install Dependencies
pip install -r requirements.txt

# Prepare first run
[python] manage.py migrate
[python] manage.py collectstatic
django-admin makemessages -a
django-admin compilemessages

## Start Gunicorn in Daemon mode
gunicorn quest.wsgi --daemon

# Reloading after changes
## Quick commands
[python] manage.py createsuperuser

## Reload Gunicorn
pkill gunicorn && gunicorn quest.wsgi --daemon

## Reload Nginx (HTTP Server)
nginx -s reload

# Useful commands
## Kill Gunicorn
pkill gunicorn

# Resources
http://mishguruorg.github.io/angular-timezone-selector/
https://docs.djangoproject.com/en/2.2/topics/db/queries/
https://docs.djangoproject.com/en/2.2/topics/i18n/timezones/
https://docs.djangoproject.com/en/2.2/ref/templates/builtins/#date
https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Deployment
https://www.digitalocean.com/community/tutorials/how-to-set-up-nginx-server-blocks-virtual-hosts-on-ubuntu-16-04
https://www.abidibo.net/blog/2016/10/07/securing-django-lets-encrypt/
https://docs.djangoproject.com/en/2.2/topics/security/
https://community.letsencrypt.org/t/type-unauthorized-detail-invalid-response-from/36183/2
https://www.agiliq.com/blog/2013/08/minimal-nginx-and-gunicorn-configuration-for-djang/
https://www.hostinger.com/tutorials/iptables-tutorial

Install gettext tool in Windows 
https://stackoverflow.com/a/45574890

Working with tags in GIT
https://gist.github.com/danielestevez/2044589

Enable sites in Nginx
https://serverfault.com/a/424456
