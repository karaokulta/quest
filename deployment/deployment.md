# quest
A gamified web app to promote writing your daily scrum

# Deployment
0. Install gettext in your server (for translations)
apt install gettext

1. Configure your Git Remote service (BitBucket) to have an Access Key
https://bitbucket.org/karaokulta/quest/admin/access-keys/

2. Clone your repo
cd /home/quest

git clone git@bitbucket.org:karaokulta/quest.git

3. TODO: Instead of going to Master, select an specific Tag to update
git pull origin master

4. Setup Nginx and Static files
STATIC_ROOT
Adjust this in quest/settings.py to use the real Path

5. Collect statics and translations
python3 manage.py collectstatic
django-admin compilemessages

6. Migrate
python3 manage.py migrate

7. Create superuser
python3 manage.py createsuperuser

4. Start Gunicorn
# Start Gunicorn daemon
gunicorn quest.wsgi --daemon

# Finish that daemon
pkill gunicorn

# Restart Nginx (to load changes in configuration)
nginx -s reload

# Info
http://honza.ca/2011/05/deploying-django-with-nginx-and-gunicorn

# tl;dr update from Git and Reload
git pull origin master
python3 manage.py collectstatic
django-admin compilemessages
pkill gunicorn && gunicorn quest.wsgi --daemon